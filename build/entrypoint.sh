#!/bin/bash

sudo /usr/sbin/nginx -g 'daemon on; master_process on;' &

/home/node/.local/bin/wssh --address=127.0.0.1 --port=7893 &

/home/node/.local/bin/gunicorn --worker-class eventlet -w 1 -c /home/node/.config/mmpm/configs/gunicorn.conf.py mmpm.wsgi:app -u node

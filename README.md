### This repo contains a Docker Setup for

# **MMPM - The MagicMirror Package Manager**

MMPM is a package manager for the [MagicMirror² Project](https://github.com/MichMich/MagicMirror).

For more info visit the [Project Website](https://github.com/Bee-Mar/mmpm).

> 👉 This docker setup is very young, so don't expect everything is working already ...

# Restrictions

❌ The Control Center of MMPM is not supported. The functionality provided there needs access to the host, partially root access. This is unwanted from inside a container.

# Installation prerequisites

You can use [MagicMirrorOS](https://github.com/guysoft/MagicMirrorOS), it contains already all the following things needed (beside the hardware):

* running raspberry pi version >2 with running raspian with LAN or WLAN access
* [Docker](https://docs.docker.com/engine/installation/)
* [docker-compose](https://docs.docker.com/compose/install/)
* running MagicMirror

You can also install and run MMPM on a linux machine (you can do this too with MagicMirror in server-only mode), for this usecase amd64 images are provided (beside the arm images needed for raspberry pi).

# Installation of this Repository

Copy the `docker-compose.yml` file in a directory of your pi, e.g. `~/mmpm`

Adjust the `docker-compose.yml` file for your needs (see explanations as comments in the file).

For starting `mmpm` go into the directory with the `docker-compose.yml` file and execute `docker-compose up -d`, for stopping execute `docker-compose down`.
You can see the logs with `docker logs mmpm`.

Use a web browser for accessing the application under `http://ip_of_your_pi:7890/`, e.g. `http://192.168.0.11:7890/`.

> The container is configured to restart automatically so after executing `docker-compose up -d` it will restart with every reboot of your pi.

# Running MagicMirror and MMPM as docker container

It make sense to run both applications as container, you can use my [docker setup for MagicMirror](https://gitlab.com/khassel/magicmirror) for this. 

You find the `docker-compose.yml` files for this setup here:

* setup for raspberry pi: [rpi_mmpm.yml](https://gitlab.com/khassel/magicmirror/-/blob/master/run/rpi_mmpm.yml)
* setup for serveronly mode: [serveronly_mmpm.yml](https://gitlab.com/khassel/magicmirror/-/blob/master/run/serveronly_mmpm.yml)

